export function removeTimeFromDate(date: string): Date {
  return new Date(date.split('T')[0]);
}
