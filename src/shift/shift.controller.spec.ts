import { Test, TestingModule } from '@nestjs/testing';
import { randomUUID } from 'crypto';

import { ShiftController } from './shift.controller';
import { ShiftService } from './shift.service';
import { DeepMockProxy, mockDeep } from 'jest-mock-extended';
import { PrismaClient } from '@prisma/client';
import { PrismaService } from '../services/prisma.service';
import { SHIFT_ROUND } from './dto/create-shift.dto';
import { HttpException, HttpStatus } from '@nestjs/common';

describe('ShiftController', () => {
  let controller: ShiftController;
  let prismaService: DeepMockProxy<PrismaClient>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ShiftController],
      providers: [PrismaService, ShiftService],
    })
      .overrideProvider(PrismaService)
      .useValue(mockDeep<PrismaClient>())
      .compile();

    controller = module.get<ShiftController>(ShiftController);
    prismaService = module.get(PrismaService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create a shift', async () => {
    const mockedShift = {
      id: 1,
      date: new Date(),
      shiftRound: SHIFT_ROUND.SECOND,
      userId: randomUUID(),
    };
    prismaService.shift.findFirst.mockResolvedValue(null);
    prismaService.shift.create.mockResolvedValue(mockedShift);

    const createShift = () =>
      controller.create({
        date: mockedShift.date.toISOString(),
        shiftRound: mockedShift.shiftRound,
        userId: mockedShift.userId,
      });

    await expect(createShift()).resolves.toBe(mockedShift);
  });

  it('should throw if a shift exist for the user that date', async () => {
    const mockedShift = {
      id: 1,
      date: new Date(),
      shiftRound: SHIFT_ROUND.SECOND,
      userId: randomUUID(),
    };
    prismaService.shift.findFirst.mockResolvedValue(mockedShift);
    prismaService.shift.create.mockResolvedValue(mockedShift);

    const createShift = () =>
      controller.create({
        date: mockedShift.date.toISOString(),
        shiftRound: mockedShift.shiftRound,
        userId: mockedShift.userId,
      });

    try {
      await createShift();
    } catch (e) {
      expect(e).toMatchObject(
        new HttpException(
          'Worker has a shift for this date',
          HttpStatus.BAD_REQUEST,
        ),
      );
    }
  });
});
