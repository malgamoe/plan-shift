import { PartialType } from '@nestjs/mapped-types';
import { CreateShiftDto } from './create-shift.dto';
import { IsNumber } from 'class-validator';

export class UpdateShiftDto extends PartialType(CreateShiftDto) {
  @IsNumber()
  id: number;
}
