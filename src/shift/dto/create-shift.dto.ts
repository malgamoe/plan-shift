import { IsDateString, IsEnum, IsUUID } from 'class-validator';

export enum SHIFT_ROUND {
  FIRST = 'first',
  SECOND = 'second',
  THIRD = 'third',
}

export class CreateShiftDto {
  @IsDateString()
  date: string;

  @IsEnum(SHIFT_ROUND)
  shiftRound: SHIFT_ROUND;

  @IsUUID()
  userId: string;
}
