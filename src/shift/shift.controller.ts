import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { ShiftService } from './shift.service';
import { CreateShiftDto } from './dto/create-shift.dto';
import { UpdateShiftDto } from './dto/update-shift.dto';
import { Prisma } from '@prisma/client';

@Controller('shift')
export class ShiftController {
  constructor(private readonly shiftService: ShiftService) {}

  @Post()
  async create(@Body() createShiftDto: CreateShiftDto) {
    const hasShiftForDate = await this.shiftService.workerHasShiftForDate(
      createShiftDto.userId,
      createShiftDto.date,
    );

    if (hasShiftForDate) {
      throw new HttpException(
        'Worker has a shift for this date',
        HttpStatus.BAD_REQUEST,
      );
    }

    return await this.shiftService.create(createShiftDto).catch((e) => {
      if (
        e instanceof Prisma.PrismaClientKnownRequestError &&
        e.code === 'P2025'
      ) {
        throw new HttpException('User does not exist', HttpStatus.NOT_FOUND);
      }
      throw e;
    });
  }

  @Get()
  findAll() {
    return this.shiftService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    const shift = await this.shiftService.findOne(+id);
    if (!shift) {
      throw new HttpException('shift does not exist', HttpStatus.NOT_FOUND);
    }
    return shift;
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateShiftDto: UpdateShiftDto) {
    return this.shiftService.update(+id, updateShiftDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.shiftService.remove(+id);
  }
}
