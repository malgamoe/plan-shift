import { Injectable } from '@nestjs/common';
import { UpdateShiftDto } from './dto/update-shift.dto';
import { CreateShiftDto } from './dto/create-shift.dto';
import { PrismaService } from '../services/prisma.service';
import { removeTimeFromDate } from '../util/date';

@Injectable()
export class ShiftService {
  constructor(private prisma: PrismaService) {}

  create(createShiftDto: CreateShiftDto) {
    const { date, shiftRound, userId } = createShiftDto;

    return this.prisma.shift.create({
      data: {
        date: removeTimeFromDate(date),
        shiftRound,
        user: { connect: { id: userId } },
      },
    });
  }

  findAll() {
    return this.prisma.shift.findMany();
  }

  findOne(id: number) {
    return this.prisma.shift.findFirst({
      where: {
        id,
      },
    });
  }

  update(id: number, updateShiftDto: UpdateShiftDto) {
    return this.prisma.shift.update({
      where: {
        id,
      },
      data: {
        date: updateShiftDto.date && removeTimeFromDate(updateShiftDto.date),
        shiftRound: updateShiftDto.shiftRound,
      },
    });
  }

  remove(id: number) {
    return this.prisma.shift.delete({
      where: {
        id,
      },
    });
  }

  async workerHasShiftForDate(userId: string, date: string): Promise<boolean> {
    const shift = await this.prisma.shift.findFirst({
      where: {
        userId,
        date: removeTimeFromDate(date),
      },
    });

    return shift !== null;
  }
}
