import { Module } from '@nestjs/common';
import { UserModule } from './user/user.module';
import { ShiftModule } from './shift/shift.module';

@Module({
  imports: [UserModule, ShiftModule],
})
export class AppModule {}
