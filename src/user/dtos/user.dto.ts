import { IsEnum, IsNotEmpty, IsString } from 'class-validator';

export enum ROLE {
  WORKER = 'worker',
  ADMIN = 'admin',
}

export class CreateUserDto {
  @IsString()
  @IsNotEmpty()
  public name: string;

  @IsEnum(ROLE)
  public role: ROLE;
}
