import { Test, TestingModule } from '@nestjs/testing';
import { UserController } from './user.controller';
import { DeepMockProxy, mockDeep } from 'jest-mock-extended';
import { PrismaClient } from '@prisma/client';
import { PrismaService } from '../services/prisma.service';
import { UserService } from './user.service';
import { randomUUID } from 'crypto';
import { ROLE } from './dtos/user.dto';

describe('UserController', () => {
  let controller: UserController;
  let prismaService: DeepMockProxy<PrismaClient>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers: [PrismaService, UserService],
    })
      .overrideProvider(PrismaService)
      .useValue(mockDeep<PrismaClient>())
      .compile();

    controller = module.get<UserController>(UserController);
    prismaService = module.get(PrismaService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('Post User', () => {
    it('should create a user', async () => {
      const mockedUser = {
        id: randomUUID(),
        name: 'Alice',
        role: ROLE.WORKER,
        shifts: [],
      };
      prismaService.user.create.mockResolvedValue(mockedUser);

      const createUser = () =>
        controller.create({
          name: mockedUser.name,
          role: mockedUser.role,
        });

      await expect(createUser()).resolves.toBe(mockedUser);
    });
  });
});
