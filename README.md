# Plan Shift

API for planning worker shifts.

It assumes workers have 3 possible shift per day, 0-8 / 8-16 / 16-24.

## Requirements

- node.js
- yarn
- docker and docker-compose

## Setup

Copy `.env.example` file to `.env`.

Run `yarn` to install packages.

Run `docker compose up` to start the postgre docker image.

Run the following commands to create the models in the db and crete the client:

```
npx prisma db push
npx prisma generate
```

## Tests

You can run tests with `yarn test`.

Also run end to end tests with `yarn test:e2e`.
