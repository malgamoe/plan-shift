import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { DeepMockProxy, mockDeep } from 'jest-mock-extended';
import { PrismaClient } from '@prisma/client';
import { PrismaService } from '../src/services/prisma.service';
import { randomUUID } from 'crypto';
import { ROLE } from '../src/user/dtos/user.dto';

describe('AppController (e2e)', () => {
  let app: INestApplication;
  let prismaService: DeepMockProxy<PrismaClient>;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
      providers: [PrismaService],
    })
      .overrideProvider(PrismaService)
      .useValue(mockDeep<PrismaClient>())
      .compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    prismaService = moduleFixture.get(PrismaService);
    await app.init();
  });

  describe('/user', () => {
    it('it should get users', () => {
      const mockedUser = {
        id: randomUUID(),
        name: 'Alice',
        role: ROLE.WORKER,
        shifts: [],
      };
      prismaService.user.findMany.mockResolvedValue([mockedUser]);

      return request(app.getHttpServer())
        .get('/user')
        .expect(200)
        .expect([mockedUser]);
    });

    it('it should create a user', async () => {
      const mockedUser = {
        id: randomUUID(),
        name: 'Alice',
        role: ROLE.WORKER,
        shifts: [],
      };
      prismaService.user.create.mockResolvedValue(mockedUser);

      const res = await request(app.getHttpServer())
        .post('/user')
        .send({ name: mockedUser.name, role: mockedUser.role });

      expect(res.status).toBe(201);
      expect(res.body).toMatchObject(mockedUser);
    });

    it('it should return an error if no body', () => {
      return request(app.getHttpServer()).post('/user').expect(400);
    });

    it('it should return an error if no name set', () => {
      return request(app.getHttpServer())
        .post('/user')
        .send({ role: ROLE.WORKER })
        .expect(400);
    });

    it('it should return an error if no role set', () => {
      return request(app.getHttpServer())
        .post('/user')
        .send({ name: 'Alice' })
        .expect(400);
    });
  });
});
